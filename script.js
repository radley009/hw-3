
let number = prompt('Please write a number:');
let userInput = parseInt(number);

if (isNaN(userInput)) {
    console.log('Invalid input. Please write a valid number.');
} else {
    let numbers = [];
    for (let i = 0; i <= userInput; i++) {
        if (i % 5 === 0) {
            numbers.push(i);
        }
    }

    if (numbers.length > 0) {
        console.log('Numbers divisible by 5:');
        console.log(numbers);
    } else {
        console.log('Sorry, no numbers.');
    }
}



/*
1.Описати своїми словами у кілька рядків, навіщо у програмуванні потрібні цикли.
цикли потрібні щоб полегшити працю з великою кількістю виконань однієї дії


2.Опишіть у яких ситуаціях ви використовували той чи інший цикл в JS.
цикл for коли хочу щоб цикл працював у певному діапазоні
цикл while коли не знаю яку кількість разів буде працювати цикл

3.Що таке явне та неявне приведення (перетворення) типів даних у JS?
явне це те що ми самі задаємо
неявне це коли тип данних сам змінюється після якихось дій у коді

 */